import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import HomePage from "./Components/HomePage";
import InvoicePage from "./Components/InvoicePage";

import "./App.css";
import "./Styles/invoicePage.css";

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/:InvoiceName" component={HomePage} />
        <Route exact path="/" component={InvoicePage} />
      </Switch>
    </Router>
  );
}

export default App;

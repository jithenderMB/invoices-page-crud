import React, { useEffect } from "react";
import { useFieldArray, useForm } from "react-hook-form";
import CloseIcon from "../Images/cross-close-icon.svg";

const initialData = {
  fromName: "",
  fromEmail: "",
  fromStreet: "",
  fromCityState: "",
  fromPincode: "",
  fromPhone: "",
  fromGstNumber: "",

  billToName: "",
  billToEmail: "",
  billToStreet: "",
  billToCityState: "",
  billToPincode: "",
  billToPhone: "",
};

const itemData = {
  itemDescription: "",
  additionalDetails: "",
  rate: 0.0,
  quantity: 1,
};

function InvoicePage() {
  const {
    register,
    handleSubmit,
    watch,
    control,
    // setValue,
    getValues,
    formState: { errors },
  } = useForm({
    defaultValues: initialData,
  });

  const { fields, append, remove } = useFieldArray({ name: "items", control });

  const submitForm = (data) => {
    // console.log(data);
  };

  useEffect(() => {
    append(itemData);
  }, []);

  const fromFields = [
    {
      title: "Name",
      name: "fromName",
      pattern: /^[a-zA-Z ]*$/,
      errorMsg: "Enter a valid name",
      check: errors.fromName,
      placeholder: "Client Name",
    },
    {
      title: "Email",
      name: "fromEmail",
      pattern: /^[^\s@]+@[^\s@]+\.[^\s@]+$/,
      errorMsg: "Enter a valid Email",
      check: errors.fromEmail,
      placeholder: "name@client.com",
    },
    {
      title: "Address",
      name: "fromStreet",
      pattern: /^[#./0-9a-zA-Z\s,-]+$/,
      errorMsg: "Enter a valid street name",
      check: errors.fromStreet,
      value: watch().fromStreet,
      placeholder: "Street",

      nameCityState: "fromCityState",
      errorMsgCityState: "Enter a valid city and state name",
      checkCityState: errors.fromCityState,
      placeholderCityState: "City,State",

      namePinCode: "fromPincode",
      patternPinCode: /^[1-9]{1}[0-9]{5}$/,
      errorMsgPinCode: "Enter a valid pin code",
      checkPinCode: errors.fromPincode,
      placeholderPinCode: "Pin Code",
    },
    {
      title: "Phone",
      name: "fromPhone",
      pattern: /^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[4-9]\d{9}$/,
      errorMsg: "Enter a valid phone number",
      check: errors.fromPhone,
      placeholder: "+91xxxxxxxxxx",
    },
    {
      title: "GST#",
      name: "fromGstNumber",
      // pattern: /^[0-9 ]{9}[A-Z]{2}$/,
      errorMsg: "Enter a valid GST number",
      // check: errors.fromGstNumber,
      check: false,
      placeholder: "123456789 RT",
    },
  ];

  const billToFields = [
    {
      title: "Name",
      name: "billToName",
      pattern: /^[a-zA-Z ]*$/,
      errorMsg: "Enter a valid name",
      check: errors.billToName,
      placeholder: "Client Name",
    },
    {
      title: "Email",
      name: "billToEmail",
      pattern: /^[^\s@]+@[^\s@]+\.[^\s@]+$/,
      errorMsg: "Enter a valid Email",
      check: errors.billToEmail,
      placeholder: "name@client.com",
    },
    {
      title: "Address",
      name: "billToStreet",
      pattern: /^[#./0-9a-zA-Z\s,-]+$/,
      errorMsg: "Enter a valid street name",
      check: errors.billToStreet,
      value: watch().billToStreet,
      placeholder: "Street",

      nameCityState: "billToCityState",
      errorMsgCityState: "Enter a valid city and state name",
      checkCityState: errors.billToCityState,
      placeholderCityState: "City,State",

      namePinCode: "billToPincode",
      patternPinCode: /^[1-9]{1}[0-9]{5}$/,
      errorMsgPinCode: "Enter a valid pin code",
      checkPinCode: errors.billToPincode,
      placeholderPinCode: "Pin Code",
    },
    {
      title: "Phone",
      name: "billToPhone",
      pattern: /^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[4-9]\d{9}$/,
      errorMsg: "Enter a valid phone number",
      check: errors.billToPhone,
      placeholder: "+91xxxxxxxxxx",
    },
  ];

  const clientDetails = ({
    title,
    name,
    pattern,
    errorMsg,
    check,
    nameCityState,
    errorMsgCityState,
    checkCityState,
    namePinCode,
    patternPinCode,
    errorMsgPinCode,
    checkPinCode,
    value,
    placeholder,
    placeholderCityState,
    placeholderPinCode,
  }) => {
    return title === "Address" ? (
      <React.Fragment key={name}>
        <div className="d-flex flex-column mt-2">
          <div className="d-flex justify-content-between">
            <label htmlFor="" className="align-self-center">
              {title}
            </label>
            <div className="w-75">
              <input
                type="text"
                className="form-control"
                {...register(name, {
                  required: true,
                  pattern: pattern,
                })}
                placeholder={placeholder}
              />
            </div>
          </div>
          <div className="d-flex justify-content-end">
            {check && <p className="text-danger w-75 m-0">* {errorMsg}</p>}
          </div>

          <div className={value ? "d-flex justify-content-end mt-2" : "d-none"}>
            <div className="w-75">
              <input
                type="text"
                className="form-control"
                {...register(nameCityState, {
                  required: true,
                  pattern: pattern,
                })}
                placeholder={placeholderCityState}
              />
              {checkCityState && (
                <p className="text-danger m-0">* {errorMsgCityState}</p>
              )}
            </div>
          </div>

          <div className={value ? "d-flex justify-content-end mt-2" : "d-none"}>
            <div className="w-75">
              <input
                type="text"
                className="form-control"
                {...register(namePinCode, {
                  required: true,
                  pattern: patternPinCode,
                })}
                placeholder={placeholderPinCode}
              />
              {checkPinCode && (
                <p className="text-danger m-0">* {errorMsgPinCode}</p>
              )}
            </div>
          </div>
        </div>
      </React.Fragment>
    ) : (
      <React.Fragment key={name}>
        <div className="d-flex flex-column mt-2">
          <div className="d-flex justify-content-between">
            <label htmlFor="" className="align-self-center">
              {title}
            </label>
            <div className="w-75">
              <input
                type="text"
                className="form-control"
                {...register(name, {
                  required: true,
                  pattern: pattern,
                })}
                placeholder={placeholder}
              />
            </div>
          </div>
          <div className="d-flex justify-content-end">
            {check && <p className="text-danger w-75 m-0">* {errorMsg}</p>}
          </div>
        </div>
      </React.Fragment>
    );
  };

  // fields.map(x => console.log(x))
  // console.log(watch().item0Quantity);
  // console.log(getValues(), "hello");
  console.log(errors);
  return (
    <form
      onSubmit={handleSubmit(submitForm)}
      className="container invoice-from-main-container p-5 mt-5 w-75"
    >
      <div className="row justify-content-center">
        <div className="col-6">
          {fromFields.map((element) => clientDetails(element))}
        </div>

        <div className="col-6">
          {billToFields.map((element) => clientDetails(element))}
        </div>
      </div>

      <div>
        <table className="mt-3">
          <thead>
            <tr className="table-heading-row">
              <th></th>
              <th className="p-0 table-heading">DESCRIPTION</th>
              <th className="p-0 table-heading text-end">
                <p className="m-0 table-padding-right-6th">RATE</p>
              </th>
              <th className="p-0 table-heading text-end ">
                <p className="m-0 table-padding-right-6th">QTY</p>
              </th>
              <th className="p-0 table-heading text-end">AMOUNT</th>
              <th className="p-0 table-heading text-end">TAX</th>
            </tr>
          </thead>
          <tbody>
            {fields.map(
              (
                { itemDescription, additionalDetails, rate, quantity },
                index
              ) => {
                return (
                  <React.Fragment key={index}>
                    <tr>
                      <td className="pt-2 table-options width-5">
                        <button className="table-item-close-button d-flex">
                          <img
                            src={CloseIcon}
                            alt="close-icon.svg"
                            className="p-0 m-0"
                          />
                        </button>
                      </td>
                      <td className="pt-2 table-options table-description">
                        <input
                          {...register(`item${index}Description`, {
                            required: "* Enter item description",
                          })}
                          type="text"
                          className="form-control"
                          defaultValue={itemDescription}
                          placeholder='Item Description'
                        />
                        <input
                          {...register(`item${index}Details`)}
                          type="text"
                          className="mt-2 form-control"
                          defaultValue={additionalDetails}
                          placeholder='Additional details'
                        />
                      </td>
                      <td className="pt-2 table-padding-left-6th table-options table-rate">
                        <input
                          {...register(`item${index}Rate`, {
                            required: "* Enter item rate",
                          })}
                          type="text"
                          className="form-control text-end"
                          defaultValue={(rate).toFixed(2)}
                          placeholder="0.00"
                        />
                      </td>
                      <td className="pt-2 table-padding-left-6th table-options table-rate">
                        <input
                          {...register(`item${index}Quantity`, {
                            required: "* Enter item rate",
                          })}
                          type="text"
                          className="form-control text-end"
                          defaultValue={quantity}
                          placeholder='0.00'
                        />
                      </td>
                      <td className="pt-2 table-options ">
                        <p
                          className="text-end"
                          {...register(`item${index}Total`)}
                        >
                          $
                          {(
                            getValues(`item${index}Rate`) *
                            getValues(`item${index}Quantity`)
                          ).toFixed(2)}
                        </p>
                      </td>
                      <td className="pt-2 table-options text-end">
                        <input type="checkbox" name="tax" />
                      </td>
                    </tr>
                  </React.Fragment>
                );
              }
            )}
          </tbody>
        </table>
      </div>

      <div className="row justify-content-center mt-4">
        <input type="submit" className="col-2 btn btn-dark" />
      </div>
    </form>
  );
}

export default InvoicePage;
